json.array!(@books) do |book|
  json.extract! book, :id, :title, :author, :publisher, :published_date, :description, :isbns, :page_count, :print_type, :category, :language
  json.url book_url(book, format: :json)
end
