Rails.application.routes.draw do
  get 'callbacks/facebook'
  get 'callbacks/google_oauth2'

    scope '/api' do
  mount_devise_token_auth_for 'User', at: '/auth'
    resources :books
  end 
end
